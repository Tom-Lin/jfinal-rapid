<#assign myParentDir="interceptor">
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first>  
package ${basepackage}.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;

<#include "/copyright_class.include" >
public class ${className}Interceptor implements Interceptor {
	
    @Override
    public void intercept(ActionInvocation ai) {
        ai.invoke();
    }
}
