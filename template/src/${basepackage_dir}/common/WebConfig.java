package  ${basepackage}.common;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
<#list tables as tt>
import ${basepackage}.model.${tt.className };
</#list>

<#include "/copyright_class.include" >
public class WebConfig extends JFinalConfig {
	
    /**
     * 配置常量
     * @param @param me    设定文件
     * @return void    返回类型
     * @throws
     */
	public void configConstant(Constants me) {
		// 加载少量必要配置，随后可用getProperty(...)获取值
		loadPropertyFile("config.properties");
		me.setDevMode(getPropertyToBoolean("devMode", false));
	}
	
	/**
	 * 配置路由
	 * @param @param me    设定文件
	 * @return void    返回类型
	 * @throws
	 */
	public void configRoute(Routes me) {
	    <#list tables as tt>
	    <#assign className = tt.className> 
	    <#assign classNameLower = className?uncap_first> 
	    me.add("${classNameLower}", ${className}.class, "${classNameLower}");
	    </#list>
	}
	
	/**
	 * 配置插件
	 * @param @param me    设定文件
	 * @return void    返回类型
	 * @throws
	 */
	public void configPlugin(Plugins me) {
		// 配置Druid数据库连接池插件
	    DruidPlugin dp = new DruidPlugin(getProperty("jdbcUrl"),getProperty("user"), getProperty("password"));
	    dp.addFilter(new StatFilter());
        WallFilter wall = new WallFilter();
        wall.setDbType("mysql");
        dp.addFilter(wall);
        me.add(dp);
        // 配置ActiveRecord插件
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp).setShowSql(true);
        me.add(arp);
        // 添加表匹配
        <#list tables as tt>
        <#assign className = tt.className> 
        <#assign sqlName = tt.sqlName> 
        arp.addMapping(${className }.TABLE_NAME, ${className }.class);
        </#list>
        
        // 添加EhCache
        me.add(new EhCachePlugin());
	}
	
	/**
	 * 配置全局拦截器
	 * @param @param me    设定文件
	 * @return void    返回类型
	 * @throws
	 */
	public void configInterceptor(Interceptors me) {
		
	}
	
	/**
	 * 配置处理器
	 * @param @param me    设定文件
	 * @return void    返回类型
	 * @throws
	 */
	public void configHandler(Handlers me) {
		
	}
	
	/**
	 * 建议使用 JFinal 手册推荐的方式启动项目
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 */
	public static void main(String[] args) {
		JFinal.start("WebRoot", 80, "/", 5);
	}
}
