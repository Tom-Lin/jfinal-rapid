package  ${basepackage}.common;

import com.jfinal.core.Controller;

<#include "/copyright_class.include" >
public class CommonController extends Controller {
	
	public void index() {
		render("/common/index.html");
	}
}
