<#assign myParentDir="controller">
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first>  
<#assign classNameLowerCase = className?lower_case>
<#assign from = basepackage?last_index_of(".")>
<#assign rootPagefloder = basepackage?substring(basepackage?last_index_of(".")+1)>
<#assign pkJavaType = table.idColumn.javaType>  
package  ${basepackage}.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StringKit;
import com.jfinal.plugin.activerecord.Page;

import ${basepackage}.model.${className};

<#include "/copyright_class.include" >
public class ${className}Controller extends Controller {
	
	/**
	 * index
	 * @param     设定文件
	 * @return void    返回类型
	 * @throws
	 */
	public void index() {
	    
	}
	
	/**
	 * list 列表
	 * @param request
	 * @param model
	 * @return
	 * @date <#if now??>${now?string('yyyy-MM-dd HH:mm:ss')}</#if>
	 */
	public void list(){
		// ==构造分页请求
		Page page = newPage(request);
		// ==执行分页查询
		List<${className}> datas=${classNameLower}.dao.findListDateByFinder(null,page,${className}.class,${classNameLower});
		setAttr("datas", datas);
		model.addAttribute("datas",datas);
		model.addAttribute("${classNameLower}",${classNameLower});
	}
	
	
	/**
	* 进入${classNameLowerCase} Web页面后直接展现第一页数据,用于初始化发访问,可以设置默认的查询条件
	*
	* @param request
	* @param model
	* @return
	* @date <#if now??>${now?string('yyyy-MM-dd HH:mm:ss')}</#if>
	*/
	public String listpre(HttpServletRequest request, Model model,${className} ${classNameLower}) throws Exception{
		return list(request, model,${classNameLower});
	}
	
	public void listexport(HttpServletRequest request,HttpServletResponse response, Model model,${className} ${classNameLower}) throws Exception{
		// ==构造分页请求
		Page page = newPage(request);
	
		File file = ${classNameLower}Service.findDataExportExcel(null,listurl, page,${className}.class,${classNameLower}Service,${classNameLower});
		String fileName="${classNameLower}"+GlobalStatic.excelext;
		downFile(response, file, fileName,true);
		return;
	}
	
	/**
	 * 查看操作
	 */
	public String show(Model model,HttpServletRequest request,HttpServletResponse response) throws Exception {
		${pkJavaType} id=request.getParameter("id");
		if(StringUtils.isNotBlank(id)){
		  ${className} ${classNameLower} = ${classNameLower}Service.find${className}ById(id);
		   model.addAttribute("${classNameLower}",${classNameLower});
		}
		return "/${rootPagefloder}/${classNameLowerCase}/${classNameLowerCase}Cru";
	}
	
	/**
	* 新增/修改save操作
	*
	* @param model
	* @param ${classNameLower}
	* @param request
	* @param response
	* @return
	* @throws Exception
	* @date <#if now??>${now?string('yyyy-MM-dd HH:mm:ss')}</#if>
	*/
	public String saveorupdate(Model model,${className} ${classNameLower},HttpServletRequest request,HttpServletResponse response) throws Exception{
		if(StringUtils.isBlank(${classNameLower}.getId())){// 新增
			<#list table.pkColumns as column>
				${classNameLower}.set${column.columnName}(SecUtils.getUUID());
			</#list>
			try {
				${classNameLower}Service.save${className}(${classNameLower});
				model.addAttribute(message, MessageUtils.ADD_SUCCESS);
				return messageurl;
			} catch (Exception e) {
				logger.error(e);
			}
			     model.addAttribute(message, MessageUtils.ADD_FAIL);
			     return messageurl;
		} else {// 修改
			try {
			<#list table.pkColumns as column>
				${classNameLower}.set${column.columnName}(${classNameLower}.getId());
			</#list>
				${classNameLower}Service.update${className}(${classNameLower});
				model.addAttribute(message, MessageUtils.EDIT_SUCCESS);
				return messageurl;
			} catch (Exception e) {
				logger.error(e);
			}
			model.addAttribute(message, MessageUtils.EDIT_WARING);
			return messageurl;
		}
		
	}
	
	/**
	 * 进入修改页面
	 */
	public String edit(Model model,HttpServletRequest request,HttpServletResponse response)  throws Exception{
		${pkJavaType} id=request.getParameter("id");
		if(StringUtils.isNotBlank(id)){
		   ${className} ${classNameLower} = ${classNameLower}Service.find${className}ById(id);
		   model.addAttribute("${classNameLower}",${classNameLower});
		}
		return "/${rootPagefloder}/${classNameLowerCase}/${classNameLowerCase}Cru";
	}
	
	/**
	 * 删除操作
	 */
	public void destroy(HttpServletRequest request) throws Exception {
		// 执行删除
		try {
			${pkJavaType} id=request.getParameter("id");
		    if(StringUtils.isNotBlank(id)){
			    ${classNameLower}Service.deleteById(id,${className}.class);
			    return new CFReturnObject(CFReturnObject.SUCCESS, MessageUtils.DELETE_SUCCESS);
			}else{
			    return new CFReturnObject(CFReturnObject.WARNING, MessageUtils.DELETE_WARNING);
			}
		} catch (Exception e) {
				logger.error(e);
		}
		return new CFReturnObject(CFReturnObject.WARNING, MessageUtils.DELETE_WARNING);
	}
	
	/**
	* 删除多条记录
	*
	* @param request
	* @param model
	* @return
	* @author Mr.Hao<Auto generate>
	* @version <#if now??> ${now?string('yyyy-MM-dd HH:mm:ss')}</#if>
	*/
	public void delMultiRecords(HttpServletRequest request, Model model) {
		String records = request.getParameter("records");
		String[] rs = records.split(",");
		int i = 0;
		for (String str : rs) {
			try {
				${classNameLower}Service.deleteById(str,${className}.class);
			} catch (Exception e) {
				if (i > 0) {
					return new CFReturnObject(CFReturnObject.ERROR, MessageUtils.DELETE_ALL_WARNING);
				}
				return new CFReturnObject(CFReturnObject.ERROR, MessageUtils.DELETE_ALL_FAIL);
			}
			i++;
		}
		return new CFReturnObject(CFReturnObject.SUCCESS, MessageUtils.DELETE_ALL_SUCCESS);
	}
}
