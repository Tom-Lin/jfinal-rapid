<#assign myParentDir="model">
<#assign className = table.className>   
<#assign classNameLower = className?uncap_first>
package ${basepackage}.model;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

<#include "/copyright_class.include" >
public class ${className} extends Model<${className}> {
	
	private static final long serialVersionUID = 1L;

	public static final String TABLE_NAME = "${table.sqlName}";
	<#list table.columns as column>
	public static final String ${column.constantName} = "${column.columnNameLowerCase}"; // ${column.columnAlias}
	</#list>
	
	public static final ${className} dao = new ${className}();
}